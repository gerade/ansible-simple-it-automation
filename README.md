# Vagrant #
vagrant up
vagrant up --provision
vagrant provision

To ssh into vagrant machine
>> vagrant ssh a-simple-automation-10

Systemd service logs
>> journalctl -u autossh1.service

Running custom playbook in vagrant node
>> ansible-playbook --private-key=~/.vagrant.d/insecure_private_key -u vagrant -i .vagrant/provisioners/ansible/inventory/vagrant_ansible_inventory playbooks/backupJenkins.yml --extra-vars "local_user=polietai"

Result:
localhost:8081
localhost:8097

#Hetzner#

# 0. known_hosts, delete entries for wge
sudo nano ~/.ssh/known_hosts

# 1. update variables and set proper IP addresses
infrastructure/provisioners/hetzner/ipmapping/stagingHosts.yml

# 2. RAW VPS setup
export ANSIBLE_HOST_KEY_CHECKING=False; ansible-playbook -i inventories/hetzner/inventory_raw provisioners/hetzner/prepareRawVPS.yml -e ansible_ssh_private_key_file=~/.ssh/id_rsa  --extra-vars  "@./provisioners/hetzner/ipmapping/stagingHosts.yml"   --ask-sudo-pass

#3. infrastructure provision
ansible-playbook -i inventories/hetzner/inventory provisioners/hetzner/hetzner.yml --user=gerawge  -e ansible_ssh_private_key_file=~/.ssh/id_rsa --extra-vars  "@./provisioners/hetzner/ipmapping/stagingHosts.yml"

Result:
http://a-simple-automation-1:80/
http://a-simple-automation-97:8080/
