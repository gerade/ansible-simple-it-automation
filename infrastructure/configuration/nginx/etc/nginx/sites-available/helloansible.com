map $http_upgrade $connection_upgrade {
 default upgrade;
 ''      close;
}

 server {
  listen          80;
  server_name     helloansible.com;

  root           /opt/tomcat/webapps/helloansible;

  error_log /var/log/nginx/localhost.error_log info;

  location / {
    proxy_pass http://web-servers-backend/helloansible/;

    access_log   /var/log/nginx/helloansible-nginx.log;
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;

    add_header Cache-Control no-cache;
  }

}
